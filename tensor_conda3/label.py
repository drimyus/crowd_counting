import scipy.io
import cv2
import os


def get_mat(path):

    return scipy.io.loadmat(path)


def get_label_in_rect(points, rect):

    ((top, left), (bottom, right)) = rect

    cnt = 0
    for p in points:
        if left < p[0] < right and bottom > p[1] > top:
            cnt += 1
    return cnt

if __name__ == '__main__':

    input_folder = './data/train/UCF_CC_50'
    i = 1

    fn_mat = str(i) + '_ann.mat'
    mat_path = os.path.join(input_folder, fn_mat)
    fn_img = str(i) + '.jpg'
    img_path = os.path.join(input_folder, fn_img)

    mat = get_mat(mat_path)
    img = cv2.imread(img_path)
    img = img[:50, :50]

    points = mat['annPoints']
    print(get_label_in_rect(points, ((0, 0), (50, 50))))

    img = cv2.resize(img, (300, 300))
    cv2.imshow("points", img)

    cv2.waitKey(0)

