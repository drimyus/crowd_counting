import cv2
import os

import feature
import label
import csv


def get_feature_and_label_data(img_path, patch_sz):

    """ read the image with its file path"""
    img = cv2.imread(img_path)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape[:2]

    rows = int(height / patch_sz)
    colomns = int(width / patch_sz)

    """ read the label data with its name """
    common_path = os.path.splitext(img_path)
    mat_path = common_path[0] + '_ann.mat'

    mat = label.get_mat(mat_path)
    points = mat['annPoints']

    data = []

    """ get the feature data and label data for each patch """
    for row in range(rows):
        for col in range(colomns):

            top = row * patch_sz
            bottom = min((row + 1) * patch_sz, height)
            left = col * patch_sz
            right = min((col + 1) * patch_sz, width)

            rect = ((top, left), (bottom, right))
            patch = gray[top:bottom, left:right]

            x_ = feature.get_features(patch)
            y_ = label.get_label_in_rect(points, rect)

            x_.append(y_)
            data.append(x_)

    return data


if __name__ == '__main__':

    folder_path = './data/train/UCF_CC_50'
    img_path = './data/train/UCF_CC_50/1.jpg'
    result_path = './tensor_conda3/training_data.csv'

    img_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg')]
    print("Number of image files for training : ", len(img_files))

    # extract the feature(X)data and label(Y) data for each image
    with open(result_path, "wb") as data_csv:
        writer = csv.writer(data_csv)

        patch_sz = 100
        len_data = 0
        for img_fn in img_files:
            img_path = os.path.join(folder_path, img_fn)
            data = get_feature_and_label_data(img_path, patch_sz)
            writer.writerows(data)
            len_data += len(data)
        print("--------------------------------")
        print("patch_size : ", patch_sz)
        print("Number of features: ", len_data)
        print("Length of features: ", len(data[0]))

        # patch_sz = 100
        # len_data = 0
        # for img_fn in img_files:
        #     img_path = os.path.join(folder_path, img_fn)
        #     data = get_feature_and_label_data(img_path, patch_sz)
        #     writer.writerows(data)
        #     len_data += len(data)
        # print("--------------------------------")
        # print("patch_size : ", patch_sz)
        # print("Number of features: ", len_data)
        # print("Length of features: ", len(data[0]))

        # patch_sz = 200
        # for img_fn in img_files:
        #     img_path = os.path.join(folder_path, img_fn)
        #     data = get_feature_and_label_data(img_path, patch_sz)
        #     writer.writerows(data)
        # print("--------------------------------")
        # print("patch_size : ", patch_sz)
        # print("Number of features: ", len_data)
        # print("Length of features: ", len(data[0]))
        #
        # patch_sz = 400
        # for img_fn in img_files:
        #     img_path = os.path.join(folder_path, img_fn)
        #     data = get_feature_and_label_data(img_path, patch_sz)
        #     writer.writerows(data)
        # print("--------------------------------")
        # print("patch_size : ", patch_sz)
        # print("Number of features: ", len_data)
        # print("Length of features: ", len(data[0]))
        #
        # print("")

    print("Writing the x_data and label_data to 'training_data.csv' Successfully !")

