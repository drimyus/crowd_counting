import numpy as np
import csv
import cv2


# Load the data from the csv file
def load_csv(filename):
    data = np.genfromtxt(filename, dtype=np.float32, delimiter=',', names=True)

    x_ = []
    y_ = []
    for i in range(len(data)):
        line = []
        for j in range(len(data[i]) - 1):
            line.append(float(data[i][j]))
        x_.append(line)
        y_.append(data[i][-1])

    return x_, y_


def check_model():

    pass


