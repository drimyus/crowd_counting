import csv

out_fn = "./test.csv"
coef_data = [[1.0, 2.0, 3.0]]

with open(out_fn, "w") as file:
    writer = csv.writer(file, delimiter=' ')
    for i in range(len(coef_data)):
        writer.writerow(coef_data[i])
