import numpy as np


path_to_csv = './training_data.csv'

data = np.genfromtxt(path_to_csv, dtype=np.float32, delimiter=',', names=True)

x_data = []
y_data = []
for i in range(len(data)):
    line = []
    for j in range(len(data[i]) - 1):
        line.append(float(data[i][j]))
    x_data.append(line)
    y_data.append(data[i][-1])


