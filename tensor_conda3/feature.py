import cv2
from skimage.feature import local_binary_pattern
from skimage.feature import greycomatrix, greycoprops
import scipy

import numpy as np


# Estimate the crowd with SIFT features
# Will return the interest points
def extract_SIFT_feature(gray):

    detector = cv2.FeatureDetector_create("SIFT")
    descriptor = cv2.DescriptorExtractor_create("SIFT")

    skp = detector.detect(gray)
    skp, sd = descriptor.compute(gray, skp)

    return [float(len(skp))]


# Extract the lbp feature from the patch
# Will return the vector with 256 length
def extract_LBP_feature(gray):

    lbp_gray = local_binary_pattern(gray, P=8, R=1, method='default')
    lbp_hist = scipy.stats.itemfreq(lbp_gray)

    histogram = np.zeros(256)
    for i in range(len(lbp_hist)):
        histogram[int(lbp_hist[i][0])] = int(lbp_hist[i][1])

    norm_hist = []
    for i in range(len(histogram)):
        norm_hist.append(histogram[i] / 256)
    return norm_hist


# Extract the GLCM feature
# will return the feature vector on which each elements are:
def extract_GLCM_feature(gray):

    xs = []
    ys = []

    glcm = greycomatrix(gray, [5], [0], 256, symmetric=True, normed=True)

    xs.append(greycoprops(glcm, 'dissimilarity')[0, 0])
    ys.append(greycoprops(glcm, 'correlation')[0, 0])

    ret = []
    ret.extend(xs)
    ret.extend(ys)
    return ret

# Extract the Fourier feature
# will the feature vector about low frequency feature
def extract_Fourier_feature(gray):

    # Take the fourier transform of the image.
    f = np.fft.fft2(gray)

    low_info = np.abs(f[:10, :10]/(gray.shape[0]*gray.shape[1]))

    return list(low_info.flat)


def get_features(patch):

    features = []

    sift = extract_SIFT_feature(patch)
    features.extend(sift)
    lbp = extract_LBP_feature(patch)
    features.extend(lbp)
    glcm = extract_GLCM_feature(patch)
    features.extend(glcm)
    fft = extract_Fourier_feature(patch)
    features.extend(fft)

    return features

if __name__ == '__main__':

    fn_input = './data/train/UCF_CC_50/1.jpg'
    gray = cv2.imread(fn_input)
    patch = gray[0:100, 0:100]

    gray = cv2.cvtColor(patch, cv2.COLOR_BGR2GRAY)

    get_features(gray)

    cv2.imshow("patch", patch)
    cv2.waitKey(0)

    print("end")





