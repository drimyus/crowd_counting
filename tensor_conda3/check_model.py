import numpy as np
import csv
import cv2
import feature
import label


# Load the data from the csv file
def load_csv(filename):

    with open(filename, 'rb') as f:
        reader = csv.reader(f)
        li = list(reader)
    coef = []
    for i in range(len(li[0])):
        coef.append(float(li[0][i]))
    return coef


def estimate_count(image, patch_sz, w_, b_, points=None):

    """ read the image with its file path"""

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape[:2]

    rows = int(height / patch_sz)
    colomns = int(width / patch_sz)

    total_cnt = 0
    """ get the feature data and label data for each patch """

    for row in range(rows):
        for col in range(colomns):
            top = row * patch_sz
            bottom = min((row + 1) * patch_sz, height)
            left = col * patch_sz
            right = min((col + 1) * patch_sz, width)

            rect = ((top, left), (bottom, right))
            patch = gray[top:bottom, left:right]

            if points is not None:
                y_ = label.get_label_in_rect(points, rect)

            x_ = feature.get_features(patch)

            # Estimate the counts on patch
            cnt = 0
            if len(x_) != len(w_) or len(b_) != 1:
                print("Error: not same type or length between w and x")
                break
            for i in range(len(x_)):
                cnt += x_[i] * w_[i]
            cnt += b_[0]
            # negative values
            cnt = max(0, cnt)

            # print(cnt - y_)
            total_cnt += cnt

    return total_cnt


def check_model_with_sample(index):

    img_path = './data/train/UCF_CC_50/' + str(index) + '.jpg'
    mat_path = './data/train/UCF_CC_50/' + str(index) + '_ann.mat'

    # Print the groud truth
    mat = label.get_mat(mat_path)
    GT = mat['annPoints']

    w_coef_fn = './tensor_conda3/coef_w.csv'
    b_coef_fn = './tensor_conda3/coef_b.csv'

    # Load the coefs
    w_data = load_csv(w_coef_fn)
    b_data = load_csv(b_coef_fn)

    print("--------------------------------")
    estimate_cnt = 0
    print("Input image path is: ", img_path)
    img = cv2.imread(img_path)
    # extract the feature(X)data and label(Y) data for each image
    # print("--------------------------------")
    patch_sz = 50
    cnts = estimate_count(img, patch_sz, w_data, b_data, GT)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts

    print("--------------------------------")
    patch_sz = 100
    cnts = estimate_count(img, patch_sz, w_data, b_data, GT)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts
    print("--------------------------------")
    # patch_sz = 200
    # cnts = estimate_count(img, patch_sz, w_data, b_data, GT)
    # print("patch_size : ", patch_sz)
    # print("counts: ", cnts)
    # estimate_cnt += cnts
    # print("--------------------------------")
    # patch_sz = 400
    # cnts = estimate_count(img, patch_sz, w_data, b_data, GT)
    # print("patch_size : ", patch_sz)
    # print("counts: ", cnts)
    # estimate_cnt += cnts

    print("result:")
    print("Estimate     : ", int(estimate_cnt / 2))
    print("Ground Truth : ", int(len(GT)))


def check_model_with_template(img_path):

    w_coef_fn = './tensor_conda3/coef_w.csv'
    b_coef_fn = './tensor_conda3/coef_b.csv'

    # Load the coefs
    w_data = load_csv(w_coef_fn)
    b_data = load_csv(b_coef_fn)

    print("--------------------------------")
    estimate_cnt = 0
    print("Input image path is: ", img_path)
    img = cv2.imread(img_path)
    # extract the feature(X)data and label(Y) data for each image
    # print("--------------------------------")
    patch_sz = 50
    cnts = estimate_count(img, patch_sz, w_data, b_data)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts

    print("--------------------------------")
    patch_sz = 100
    cnts = estimate_count(img, patch_sz, w_data, b_data)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts
    print("--------------------------------")
    patch_sz = 200
    cnts = estimate_count(img, patch_sz, w_data, b_data)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts
    print("--------------------------------")
    patch_sz = 400
    cnts = estimate_count(img, patch_sz, w_data, b_data)
    print("patch_size : ", patch_sz)
    print("counts: ", cnts)
    estimate_cnt += cnts

    print("result:")
    print("Estimate     : ", int(estimate_cnt / 4))


if __name__ == '__main__':

    for index in range(1, 50):
        check_model_with_sample(index)
        cv2.waitKey(0)
    # check_model_with_template('./data/test/sample_images/crowd-at-a-stadium-in-johannesburg-south-africa-for-rugby.jpg')
