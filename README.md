# README #

This README would normally document whatever steps are necessary to get your application up and running.

opencv == 3x

### What is this repository for? ###

* This is implementation for the crowd counting based dense crowd.
* The paper can be found [here](https://arxiv.org/pdf/1507.08445.pdf)
* The detailed project can be found [here](http://crcv.ucf.edu/projects/crowdCounting/index.php)

### How do I get set up? ###

* requirements

    preparing the training data:
        opencv 3x
        python 3.5

    training:
        conda3 x64
        tensorflow 1.0.0

    testing:
        conda3 x64
        opencv 3x

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact