"""
pre_training

    Load the images from the folder
    Extract the feature vector
    Create the training data as .csv files with
                                features, labels, file names

"""

import tensorflow as tf
import os
import numpy as np
import sys
import tarfile
import cv2
import csv
from six.moves import urllib
from node_lookup import NodeLookup
import label

MODEL_DIR = './model'
DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'

CONFIG = {
    'num_top_predictions': 5,
    'x_data_csv': 'total_x.csv',
    'fn_csv': 'total_fn.csv',
    'label_csv': 'total_y.csv'
}


def create_graph():

    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    with tf.gfile.FastGFile(os.path.join(
            MODEL_DIR, 'classify_image_graph_def.pb'), 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')


def maybe_download_and_extract():
    """Download and extract model tar file."""
    dest_directory = MODEL_DIR
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = DATA_URL.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (
                filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()
        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
    tarfile.open(filepath, 'r:gz').extractall(dest_directory)


def run_inference_on_image(img_path):
    """Runs inference on an image.
        Args:
            img_path: Image file name.

    Returns:
        Nothing
    """
    if not tf.gfile.Exists(img_path):
        tf.logging.fatal('File does not exist %s', img_path)
    image_data = tf.gfile.FastGFile(img_path, 'rb').read()

    # Creates graph from saved GraphDef.
    create_graph()

    with tf.Session() as sess:
        ###
        # Some useful tensors:
        # 'softmax:0': A tensor containing the normalized prediction across
        #               1000 labels.

        # 'pool_3:0': A tensor containing the next-to-last layer containing
        #               2048 float description of the image.

        # 'DecodeJpeg/contents:0': A tensor containing a string providing
        #               JPEG encoding of the image.

        # Runs the softmax tensor by feeding the image_data as input to the graph.
        ###
        softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
        predictions = sess.run(softmax_tensor,
                               {'DecodeJpeg/contents:0': image_data})
        predictions = np.squeeze(predictions)

        # Creates node ID --> English string lookup.
        node_lookup = NodeLookup()

        top_k = predictions.argsort()[-CONFIG['num_top_predictions']:][::-1]
        for node_id in top_k:
            human_string = node_lookup.id_to_string(node_id)
            score = predictions[node_id]
            print('%s (score = %.5f)' % (human_string, score))


sess, softmax_tensor = None, None


def get_feature_from_image(img_path):
    """Runs extract the feature from the image.
        Args:
            img_path: Image file name.

    Returns:
        predictions:
            2048 * 1 feature vector
    """
    global sess, softmax_tensor

    if not tf.gfile.Exists(img_path):
        tf.logging.fatal('File does not exist %s', img_path)
    image_data = tf.gfile.FastGFile(img_path, 'rb').read()

    predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    return predictions


def get_feature_from_patch(patch):

    global sess, softmax_tensor
    image_data = cv2.imencode(".jpg", patch)[1].tostring()

    predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    return list(predictions)


def get_feature_and_label_from_img_path(img_path, patch_sz):

    """ read the image with its file path"""
    img = cv2.imread(img_path)
    height, width = img.shape[:2]
    rows = int(height / patch_sz)
    colomns = int(width / patch_sz)

    """ read the label data with its name """
    common_path = os.path.splitext(img_path)
    mat_path = common_path[0] + '_ann.mat'

    mat = label.get_mat(mat_path)
    points = mat['annPoints']

    data = []
    """ get the feature data and label data for each patch """
    for row in range(rows):
        for col in range(colomns):

            top = row * patch_sz
            bottom = min((row + 1) * patch_sz, height)
            left = col * patch_sz
            right = min((col + 1) * patch_sz, width)

            rect = ((top, left), (bottom, right))
            patch = img[top:bottom, left:right]

            x_ = get_feature_from_patch(patch)
            y_ = label.get_label_in_rect(points, rect)

            x_.append(y_)
            data.append(x_)

    return data


def main():

    global sess, softmax_tensor

    maybe_download_and_extract()
    # fn_img = './data/ *.jpg'
    # run_inference_on_image(fn_img)

    # Creates graph from saved GraphDef.
    create_graph()
    sess = tf.Session()
    softmax_tensor = sess.graph.get_tensor_by_name('pool_3:0')

    print("--------------------------------------------------")
    print("Extract the feature with using Inception_V3 model.")
    print("--------------------------------------------------")

    # image file list on target folder
    folder_path = '../data/train/UCF_CC_50'
    result_path = './linear_regression/training_data.csv'
    img_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg')]
    print("Number of image files for training : ", len(img_files))

    # extract the feature(X)data and label(Y) data for each image
    with open(result_path, "w") as data_csv:
        writer = csv.writer(data_csv)

        patch_sz = 100
        len_data = 0
        for img_fn in img_files:
            print(img_fn)
            img_path = os.path.join(folder_path, img_fn)
            data = get_feature_and_label_from_img_path(img_path, patch_sz)
            writer.writerows(data)
            len_data += len(data)
        print("---")
        print("patch_size : ", patch_sz)
        print("Number of features: ", len_data)
        print("Length of a feature: ", len(data[0]))

        patch_sz = 200
        len_data = 0
        for img_fn in img_files:
            print(img_fn)
            img_path = os.path.join(folder_path, img_fn)
            data = get_feature_and_label_from_img_path(img_path, patch_sz)
            writer.writerows(data)
            len_data += len(data)
        print("---")
        print("patch_size : ", patch_sz)
        print("Number of features: ", len_data)
        print("Length of a feature: ", len(data[0]))

    print("Writing the x_data and label_data to 'training_data.csv' Successfully !")

if __name__ == '__main__':

    main()
