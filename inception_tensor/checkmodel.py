# import pre_process

import tensorflow as tf
import os
import cv2
import numpy as np
import sys
import tarfile
import csv
import datetime

import label
import scale_size
from six.moves import urllib


sess, softmax_tensor = None, None
w_data, b_data = [], []

MODEL_DIR = './model'
DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'


def load_coefs():

    global w_data, b_data
    w_path = './linear_regression/coef_w.csv'
    b_path = './linear_regression/coef_b.csv'
    w_ = []
    b_ = []

    print("loading the w_ coefs...")
    # data = np.genfromtxt(w_path, dtype=np.float32, delimiter=',', names=True)
    # for i in range(len(data)):
    #     w_.append(float(data[i]))

    with open(w_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            w_ = row
            # only first line
            break
    for i in range(len(w_)):
        w_data.append(float(w_[i]))

    print("loading the b_ coefs...")
    # data = np.genfromtxt(b_path, dtype=np.float32, delimiter=',', names=True)
    # for i in range(len(data)):
    #     b_.append(float(data[i]))

    with open(b_path, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            b_ = row
            # only first line
            break

    for i in range(len(b_)):
        b_data.append(float(b_[i]))


def maybe_download_and_extract():
    """Download and extract model tar file."""
    dest_directory = MODEL_DIR
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = DATA_URL.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (
                filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
    tarfile.open(filepath, 'r:gz').extractall(dest_directory)


def create_graph():

    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    with tf.gfile.FastGFile(os.path.join(
            MODEL_DIR, 'classify_image_graph_def.pb'), 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')


def get_feature_from_patch(patch):

    global sess, softmax_tensor
    image_data = cv2.imencode(".jpg", patch)[1].tostring()

    predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    return list(predictions)


def estimate_counts(img_path, scale):

    global w_data, b_data
    start = datetime.datetime.now()

    """ read the image with its file path"""
    img = cv2.imread(img_path)
    if int(scale) == 0:
        scale = scale_size.extract_feature_scale(img)

    height, width = img.shape[:2]
    print("    scale :", scale, "   size :", "w-", width, "x h-", height)

    """ estimate counts for each patch """
    """ patch_size = 50 """
    patch_sz = int(50 * scale)

    rows = int(height / patch_sz)
    colomns = int(width / patch_sz)

    total_est_50 = 0
    for row in range(rows):
        for col in range(colomns):
            top = row * patch_sz
            bottom = min((row + 1) * patch_sz, height)
            left = col * patch_sz
            right = min((col + 1) * patch_sz, width)

            patch = img[top:bottom, left:right]

            x_ = get_feature_from_patch(patch)

            est = 0
            for i in range(len(x_)):
                est += x_[i] * w_data[i]
            est += b_data

            total_est_50 += max(est, 0)

    """ patch_size = 100 """
    patch_sz = int(100 * scale)

    rows = int(height / patch_sz)
    colomns = int(width / patch_sz)

    total_est_100 = 0
    for row in range(rows):
        for col in range(colomns):
            top = row * patch_sz
            bottom = min((row + 1) * patch_sz, height)
            left = col * patch_sz
            right = min((col + 1) * patch_sz, width)

            patch = img[top:bottom, left:right]

            x_ = get_feature_from_patch(patch)

            est = 0
            for i in range(len(x_)):
                est += x_[i] * w_data[i]
            est += b_data

            total_est_100 += max(est, 0)

    total_est = (total_est_50 + total_est_100) / 2

    end = datetime.datetime.now()
    print("    duration: ", (end - start))
    return int(total_est)


def GT_counts(img_path):

    """ read the label data with its name """
    common_path = os.path.splitext(img_path)
    mat_path = common_path[0] + '_ann.mat'

    mat = label.get_mat(mat_path)
    points = mat['annPoints']

    return len(points)


def test_model_with_image(fn_image, scale):

    global sess, softmax_tensor
    global w_data, b_data

    load_coefs()

    # Creates graph from saved GraphDef.
    create_graph()
    sess = tf.Session()
    softmax_tensor = sess.graph.get_tensor_by_name('pool_3:0')

    print("--------------------------------------------------")
    print("Extract the feature with using Inception_V3 model.")
    print("--------------------------------------------------")

    fn_list = ["3734360895_bfb8ba3a83_o.jpg",
               "a1-inauguration-new.jpg",
               "Crowd.jpg",
               "crowd-at-a-stadium-in-johannesburg-south-africa-for-rugby.jpg",
               "crowd-pic-small.jpg",
               "download (7).jpg",
               "download (8).jpg",
               "e8b8012397764bdba7cab4caa188e66f.jpg",
               "images (3).jpg",
               "Louisville_Crowd-6.0.JPG",
               "Obama_inaugural_address.jpg"]

    for fn in fn_list:
        print("---", fn)
        path = '../data/test/sample_images/' + fn
        est = estimate_counts(path, scale)
        print("    Est : ", est)

    # fn = "crowd-at-a-stadium-in-johannesburg-south-africa-for-rugby.jpg"
    # print("---", fn)
    # path = '../data/test/sample_images/' + fn
    # est = estimate_counts(path, 1.0)
    # print("Est : ", est)


def check_model(scale):

    global sess, softmax_tensor
    global w_data, b_data

    # maybe_download_and_extract()
    # fn_img = './data/ *.jpg'
    # run_inference_on_image(fn_img)

    load_coefs()

    # Creates graph from saved GraphDef.
    create_graph()
    sess = tf.Session()
    softmax_tensor = sess.graph.get_tensor_by_name('pool_3:0')

    print("--------------------------------------------------")
    print("Extract the feature with using Inception_V3 model.")
    print("--------------------------------------------------")

    # image file list on target folder
    folder_path = '../data/train/UCF_CC_50'

    img_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg')]
    print("Number of image files for testing : ", len(img_files))

    for img_fn in img_files:
        print()
        sample_img_path = os.path.join(folder_path, img_fn)

        est = estimate_counts(sample_img_path, scale=scale)
        GT = GT_counts(sample_img_path)
        print("--- ", img_fn, " | EST : ", est, " GT : ", GT, " error : ", est - GT, " percent %: ", (est - GT)*100/GT)


if __name__ == '__main__':

    # Vailidation with training Image
    # check_model(2.0)

    # Testing with not training data
    fn_image = '../data/test/sample_images/3734360895_bfb8ba3a83_o.jpg'
    test_model_with_image(fn_image, 0)

