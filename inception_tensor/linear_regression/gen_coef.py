import csv
import tensorflow as tf
import numpy as np
import math


def xaver_init(n_inputs, n_outputs, uniform=True):
    if uniform:
        init_range = tf.sqrt(6.0 / (n_inputs + n_outputs))
        return tf.random_uniform_initializer(-init_range, init_range)

    else:
        stddev = tf.sqrt(3.0 / (n_inputs + n_outputs))
        return tf.truncated_normal_initializer(stddev=stddev)


# Load the data from the csv file
def load_csv(filename):
    data = np.genfromtxt(filename, dtype=np.float32, delimiter=',', names=True)

    x_ = []
    y_ = []
    for i in range(len(data)):
        line = []
        for j in range(len(data[i]) - 1):
            line.append(float(data[i][j]))
        x_.append(line)
        y_.append(data[i][-1])

    return x_, y_


def write_coef_csv(out_fn, coef_data):

    with open(out_fn, "w") as file:
        writer = csv.writer(file, delimiter=',')
        for i in range(len(coef_data)):
            line = []
            for j in range(len(coef_data[i])):
                line.append(float(coef_data[i][j]))
            writer.writerow(line)


def gen_coef(input_fn, ckp_fn, w_fn, b_fn):

    print('Loading data...')
    # Load the training data covert float32 for training
    """ Load data"""
    print("Data loading")
    x_data, y_data = load_csv(input_fn)

    x_data = np.transpose(x_data)
    y_data = np.transpose(y_data)

    x_data = x_data.astype(dtype=np.float32)
    y_data = y_data.astype(dtype=np.float32)

    print(" - Length of X_data: ", len(x_data), len(x_data[0]))
    print(" - Length of Y_data: ", len(y_data), 1)
    print(" - W'shape is:", len(x_data[0]), '*', 1)
    print(" - b'shape is:", 1, '* 1')

    """ configure """
    print("Configure the model.")
    W = tf.Variable(tf.random_uniform([1, len(x_data)], -1, 1))
    b = tf.Variable(tf.random_uniform([1], -1, 1))

    # Initializing the variables
    init = tf.initialize_all_variables()

    sess = tf.Session()
    sess.run(init)
    saver = tf.train.Saver()

    """ Load the coefs W, b from the model """
    print('Load weight coefs(W, b) from the model')
    saver.restore(sess, ckp_fn)
    ret_w = sess.run(W)
    ret_b = sess.run(b)

    """ Write the coefs as a csv file """
    print("Write the coefs")
    # Write the W weight
    write_coef_csv(w_fn, ret_w)
    # Write the b coef
    with open(b_fn, "w") as file:
        writer = csv.writer(file, delimiter=',')
        for i in range(len(ret_b)):
            line = []
            line.append(float(ret_b[i]))
            writer.writerow(line)
    print('Finished saving coefs successfully')


if __name__ == '__main__':

    input_fn = './training_data.csv'
    check_point_fn = './model_bin.ckpt'
    w_coef_fn = './coef_w.csv'
    b_coef_fn = './coef_b.csv'
    gen_coef(input_fn, check_point_fn, w_coef_fn, b_coef_fn)
    print("write coefs to '" + w_coef_fn + "' and '" + b_coef_fn + "'.")
