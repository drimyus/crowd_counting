import tensorflow as tf
import numpy as np
import csv


# Load the data from the csv file
def load_csv(filename):
    data = np.genfromtxt(filename, dtype=np.float32, delimiter=',', names=True)

    x_ = []
    y_ = []
    for i in range(len(data)):
        line = []
        for j in range(len(data[i]) - 1):
            line.append(float(data[i][j]))
        x_.append(line)
        y_.append(data[i][-1])

    return x_, y_


# Main progress for training
def main_train(path_to_csv, ckp_path, alpha=0.0000001, epochs=200001, stride=500):

    # Load the training data covert float32 for training
    """ Load data"""
    print("Data loading")
    x_data, y_data = load_csv(path_to_csv)

    x_data = np.transpose(x_data)
    y_data = np.transpose(y_data)

    x_data = x_data.astype(dtype=np.float32)
    y_data = y_data.astype(dtype=np.float32)

    print(" - Length of X_data: ", len(x_data), len(x_data[0]))
    print(" - Length of Y_data: ", len(y_data), 1)
    print(" - W'shape is:", len(x_data[0]), '*', 1)
    print(" - b'shape is:", 1, '* 1')

    # Configure the model
    """ configure """
    print("configure")
    W = tf.Variable(tf.random_uniform([1, len(x_data)], -1, 1))
    b = tf.Variable(tf.random_uniform([1], -1, 1))

    hypothesis = tf.add(tf.matmul(W, x_data), b)

    cost = tf.reduce_mean(tf.square(hypothesis - y_data))

    a = tf.Variable(alpha)  # learning rate, alpha
    optimizer = tf.train.GradientDescentOptimizer(a)
    train = optimizer.minimize(cost)  # goal is minimize cost

    # Initializing the variables
    """ Init """
    print("initializing the model")
    init = tf.global_variables_initializer()

    sess = tf.Session()
    sess.run(init)
    saver = tf.train.Saver()

    """CHECKPOINT"""
    print('Load learning checkpoint...')
    saver.restore(sess, ckp_path)

    for step in range(epochs):
        sess.run(train)
        if step % stride == 0:
            print(step, sess.run(cost))
        saver.save(sess, ckp_path)

    print("Optimization Finished!")


if __name__ == '__main__':

    input_path = './training_data.csv'
    out_path = './model_bin.ckpt'
    main_train(input_path, out_path, alpha=0.001, epochs=200001, stride=100)
